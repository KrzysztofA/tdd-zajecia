package pl.sda.tdd.test.bank;

import org.junit.Before;
import org.junit.Test;
import pl.sda.tdd.bank.Bank;
import pl.sda.tdd.bank.Konto;

import static org.junit.Assert.assertEquals;

public class BankTest {

    private Konto kontoAndrzeja;
    private Konto kontoMoniki;

    private Bank bank;

    @Before
    public void setup(){
        kontoAndrzeja = new Konto(10000, 1000, 1234);
        kontoMoniki = new Konto(0, 1000, 5555);
        bank = new Bank();
        bank.dodajKonto(kontoAndrzeja);
        bank.dodajKonto(kontoMoniki);
    }

    @Test
    public void udanyPrzelew(){
        bank.przelejKwote(kontoAndrzeja, kontoMoniki, 999, 1234);
        assertEquals(999, kontoMoniki.pobierzStanKonta());
        assertEquals(9001, kontoAndrzeja.pobierzStanKonta());
    }

    @Test
    public void nieudanyPrzelewPrzyBrakuSrodkow(){
        bank.przelejKwote(kontoMoniki, kontoAndrzeja, 5, 5555);
        assertEquals(0, kontoMoniki.pobierzStanKonta());
        assertEquals(10000, kontoAndrzeja.pobierzStanKonta());
    }

    @Test
    public void nieudanyPrzelewPrzyBrakuAutoryzacji(){
        bank.przelejKwote(kontoAndrzeja, kontoMoniki, 100, 1233);
        assertEquals(10000, kontoAndrzeja.pobierzStanKonta());
        assertEquals(0, kontoMoniki.pobierzStanKonta());
    }

    @Test
    public void nieudanyPrzelewPrzyPrzekroczonymLimicie(){
        bank.przelejKwote(kontoAndrzeja, kontoMoniki, 1001, 1234);
        assertEquals(10000, kontoAndrzeja.pobierzStanKonta());
        assertEquals(0, kontoMoniki.pobierzStanKonta());
    }
}
