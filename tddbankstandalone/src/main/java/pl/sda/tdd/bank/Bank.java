package pl.sda.tdd.bank;

import java.util.ArrayList;
import java.util.List;

public class Bank {
    private List<Konto> kontaWBanku;

    public Bank(){
        kontaWBanku = new ArrayList<>();
    }

    public void dodajKonto(Konto konto) {
        kontaWBanku.add(konto);
    }

    public void przelejKwote(Konto nadawca, Konto odbiorca, int kwota, int kodNadawcy) {
        if(nadawca.pobierzKwote(kwota, kodNadawcy))
            odbiorca.wplacKwote(kwota);
    }
}
