package pl.sda.tdd.bank;

public class Konto {
    private int stanKonta;
    private int limitDzienny;
    private int kodPin;

    public Konto(int stanKonta, int limitDzienny, int kodPin) {
        this.stanKonta = stanKonta;
        this.limitDzienny = limitDzienny;
        this.kodPin = kodPin;
    }

    public boolean pobierzKwote(int kwota, int kodPin) {
        if(kwota<=stanKonta && kwota<=limitDzienny && this.kodPin == kodPin){
            stanKonta -= kwota;
            return true;
        }
        return false;
    }

    public int pobierzStanKonta() {
        return this.stanKonta;
    }

    public void wplacKwote(int kwota) {
        this.stanKonta += kwota;
    }
}
